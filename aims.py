import numpy as np
def std(x): # we define our standard deviation
    average = np.mean(x)
    deviasum = 0.0 # we make the initial sum to be zero
    for j in range(len(x)):
        devia = float(x[j] - average)**2 # we make it to be a floating point
        deviasum += devia
    return np.sqrt(deviasum/len(x)) # to display the standard deviation

import numpy as np

if __name__=='__main__':   
    print "Welcome to the AIMS module"

def std(x):
    xprime = np.mean(x)
    deviasum=0.0
    for j in range(len(x)):
        devia_2 = (x[j] - xprime)**2
        deviasum =deviasum + devia_2
    return np.sqrt(deviasum/len(x))

def avg_range(sets):
    files=[] # this specify that the name file  is a list
    ranges=[]
    for location in sets: # to specify what should be executed if sets contains location
        files.append(open(location))
    for entry in files:
        for line in entry:
            if 'Range' in line:
                ranges.append(float(line[7]))
    return sum(ranges)/len(ranges)

