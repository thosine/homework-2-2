from nose.tools import assert_equal

import aims
def test_equaposi_ints(): # for two integers
    numbers = [33,33]
    obs = aims.std(numbers) # this define the observed value
    exp = 0.0 # the specified the expected value
    assert_equal(obs,exp)



def test_nega_ints():
    numbers = [-4,-5]
    obs = aims.std(numbers)
    exp = 0.5
    assert_equal(obs,exp)


def test_posi_ints():
    numbers = [12,27]
    obs = aims.std(numbers)
    exp = 7.5
    assert_equal(obs,exp)


def test_nega_posi():
    numbers = [-2,4]
    obs = aims.std(numbers)
    exp = 3.0
    assert_equal(obs,exp)


def test_avg1():
    files = ['data/bert/audioresult-00215']
    abs = aims.avg_range(files)
    exp = 5.0
    assert_equal(abs, exp)

def test_avg2():
    files = ['data/bert/audioresult-00451']
    abs = aims.avg_range(files)
    exp = 6.0
    assert_equal(abs, exp)

def test_avg3():
    files = ['data/bert/audioresult-00557']
    abs = aims.avg_range(files)
    exp = 2.0
    assert_equal(abs, exp)

